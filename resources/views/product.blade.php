
<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

       <title>МЕГАКОМП</title><link rel="shortcut icon" type="image/png" href="{{asset('img/fav.png')}}"/>
        <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('js/loader.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/mainpage.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/accordeon.js')}}"></script>

        <!-- Fonts -->
        <link rel="stylesheet" href="{{asset('styles/style.css')}}">
        <link href="{{asset('Montserrat/stylesheet-montserrat.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('styles/loader.css')}}">
        <link rel="stylesheet" href="{{asset('styles/product.css')}}">
    </head>
    <body>
        <header>
        <div class="loaderArea">
        <div class="sk-circle" id="preloader">
            <div class="sk-circle1 sk-child"></div>
            <div class="sk-circle2 sk-child"></div>
            <div class="sk-circle3 sk-child"></div>
            <div class="sk-circle4 sk-child"></div>
            <div class="sk-circle5 sk-child"></div>
            <div class="sk-circle6 sk-child"></div>
            <div class="sk-circle7 sk-child"></div>
            <div class="sk-circle8 sk-child"></div>
            <div class="sk-circle9 sk-child"></div>
            <div class="sk-circle10 sk-child"></div>
            <div class="sk-circle11 sk-child"></div>
            <div class="sk-circle12 sk-child"></div>
        </div>
    </div>
            <a href="{{ url('/') }}" class="top-title-header">
                <div class="content">
                    <div class="title-header">
                <h1>МЕГАКОМП</h1>
                <p class="sub-title-header">
                    Интернет-магазин компьютерной техники
                </p>
                </div>
                </div>
            </a>
            <div class="full-width-header">
            <div class="content">
                <div class="header-content">
                        <div class="links">
                            <a href="{{ url('/catalog') }}">Каталог</a>
                           <a href="{{ url('/sales') }}">Скидки</a>
                             <a href="{{ url('/information') }}">Полезная информация</a>
                           <a href="{{ url('/about-us') }}">О нас</a>
                        </div>
                        <div class="login-links">
                        @if (Route::has('login'))
                    
                    @auth
                        <a href="{{ url('/home') }}">Домой</a>
                    @else
                        <a href="{{ route('login') }}">Вход</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Регистрация</a>
                        @endif
                    @endauth
            
                 @endif    
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </header>
<section id="product-all-info">
    <div class="content">
        <div class="all-block-info">
            <div class="product-block">
                <div class="info-image-block">
                    <img src="{{asset('img/asus-wh.jpg')}}" alt="">
                </div>
                <div class="buy-and-price-block">
                    <h2 class="info-product-name">Имя</h2>
                    <p class="info-product-code">Код</p>
                    <p class="info-type">Тип</p>
                    <p class="info-price">2 000 ₽</p>
                    <p class="in-stock">В наличии: <span>есть</span></p>
                    <div class="buy-button">
                        <p>Купить</p>
                        <img src="{{asset('img/korzina.svg')}}" alt="">
                    </div>
                </div>
            </div>
            <div class="characters-info">
            <div id="main__content">
                <div id="servise__name" class="main__content-item">
                    <p class="main__content-title">Классификация</p>
                    <img class="arrow" src="{{asset('img/line5.svg')}}" alt="">
                    <div class="subcontent">
                        <p class="parametr">Параметр</p>
                    </div>
                </div>
                <div id="servise__who" class="main__content-item">
                    <p class="main__content-title">Внешний вид</p>
                    <img class="arrow" src="img/line5.svg" alt="">
                    <div class="subcontent">
                    <p class="parametr">Параметр</p>
                    </div>
                </div>
                <div id="servise__list-doc" class="main__content-item">
                    <p class="main__content-title">Корпус и устройства ввода</p>
                    <img class="arrow" src="{{asset('img/line5.svg')}}" alt="">                    
                    <div class="subcontent">
                    <p class="parametr">Параметр</p>
                    </div>
                </div>
                <div id="servise__procedure" class="main__content-item">
                    <p class="main__content-title">Экран</p>
                    <img class="arrow" src="{{asset('img/line5.svg')}}" alt="">
                    <div class="subcontent">
                    <p class="parametr">Параметр</p>

                    </div>
                </div>
                <div id="servise__forms" class="main__content-item">
                    <p class="main__content-title">Процессор</p>
                    <img class="arrow" src="{{asset('img/line5.svg')}}" alt="">
                    <div class="subcontent">
                    <p class="parametr">Параметр</p>
                    </div>
                </div>
                <div id="servise__norms" class="main__content-item">
                    <p class="main__content-title">Оперативная память</p>
                    <img class="arrow" src="{{asset('img/line5.svg')}}" alt="">
                    <div class="subcontent">
                    <p class="parametr">Параметр</p>
                    </div>
                </div>
                <div id="servise__informs" class="main__content-item">
                    <p class="main__content-title">Графический ускоритель</p>
                    <img class="arrow" src="{{asset('img/line5.svg')}}" alt="">
                    <div class="subcontent">
                    <p class="parametr">Параметр</p>
                    </div>
                </div>
                <div id="servise__informs" class="main__content-item">
                    <p class="main__content-title">Накопители данных</p>
                    <img class="arrow" src="{{asset('img/line5.svg')}}" alt="">
                    <div class="subcontent">
                    <p class="parametr">Параметр</p>
                    </div>
                </div>
                <div id="servise__informs" class="main__content-item">
                    <p class="main__content-title">Встроенное дополнительное оборудование</p>
                    <img class="arrow" src="{{asset('img/line5.svg')}}" alt="">
                    <div class="subcontent">
                    <p class="parametr">Параметр</p>
                    </div>
                </div>
                <div id="servise__informs" class="main__content-item">
                    <p class="main__content-title">Интернет/передача данных</p>
                    <img class="arrow" src="{{asset('img/line5.svg')}}" alt="">
                    <div class="subcontent">
                    <p class="parametr">Параметр</p>
                    </div>
                </div>
                <div id="servise__informs" class="main__content-item">
                    <p class="main__content-title">Интерфейсы/разъемы</p>
                    <img class="arrow" src="{{asset('img/line5.svg')}}" alt="">
                    <div class="subcontent">
                    <p class="parametr">Параметр</p>
                    </div>
                </div>
                <div id="servise__informs" class="main__content-item">
                    <p class="main__content-title">Питание</p>
                    <img class="arrow" src="{{asset('img/line5.svg')}}" alt="">
                    <div class="subcontent">
                    <p class="parametr">Параметр</p>
                    </div>
                </div>
                <div id="servise__informs" class="main__content-item">
                    <p class="main__content-title">Габариты, вес</p>
                    <img class="arrow" src="{{asset('img/line5.svg')}}" alt="">
                    <div class="subcontent">
                    <p class="parametr">Параметр</p>
                    </div>
                </div>
                
                </div>
            </div>
            
            
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="content">
        <div class="footer-content">
            <div class="footer-column">
                <h4>Навигация</h4>
                <ul>
                    <li>
                        <a href="">Каталог</a>
                    </li>
                    <li>
                        <a href="">Скидки</a>
                    </li>
                    <li>
                        <a href="">Полезная информация</a>
                    </li>
                    <li>
                        <a href="">О нас</a>
                    </li>
                </ul>
            </div>
            <div class="footer-column">
                <h4>Авторизация</h4>
                
                <ul>
                @if (Route::has('login'))
                    
                    @auth
                    <li>
                        <a href="{{ url('/home') }}">Домой</a>
                        </li>
                    @else
                    <li>
                        <a href="{{ route('login') }}">Вход</a>
                        </li>

                        @if (Route::has('register'))
                        <li>
                            <a href="{{ route('register') }}">Регистрация</a>
                            </li>
                        @endif
                    @endauth
            
                 @endif    
                </ul>
            </div>
            <div class="footer-column column-razr">
               <div class="razr">
                   <p>Разработка сайта</p>
                   <p>Кузнецов Степан 2020</p>
               </div>
            </div>
        </div>
    </div>
</footer>




       
        
    </body>
</html>
