
<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

       <title>МЕГАКОМП</title><link rel="shortcut icon" type="image/png" href="{{asset('img/fav.png')}}"/>
        <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('js/loader.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/mainpage.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/accordeon.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/product.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/minheight.js')}}"></script>

        <!-- Fonts -->
        <link rel="stylesheet" href="{{asset('styles/style.css')}}">
        <link href="{{asset('Montserrat/stylesheet-montserrat.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('styles/loader.css')}}">
        <link rel="stylesheet" href="{{asset('styles/product.css')}}">
    </head>
    
    <body>
        

        <header>
        <div class="loaderArea">
        <div class="sk-circle" id="preloader">
            <div class="sk-circle1 sk-child"></div>
            <div class="sk-circle2 sk-child"></div>
            <div class="sk-circle3 sk-child"></div>
            <div class="sk-circle4 sk-child"></div>
            <div class="sk-circle5 sk-child"></div>
            <div class="sk-circle6 sk-child"></div>
            <div class="sk-circle7 sk-child"></div>
            <div class="sk-circle8 sk-child"></div>
            <div class="sk-circle9 sk-child"></div>
            <div class="sk-circle10 sk-child"></div>
            <div class="sk-circle11 sk-child"></div>
            <div class="sk-circle12 sk-child"></div>
        </div>
    </div>
            <a href="{{ url('/') }}" class="top-title-header">
                <div class="content">
                    <div class="title-header">
                <h1>МЕГАКОМП</h1>
                <p class="sub-title-header">
                    Интернет-магазин компьютерной техники
                </p>
                </div>
                </div>
            </a>
            <div class="full-width-header">
            <div class="content">
                <div class="header-content">
                        <div class="links">
                            <a href="{{ url('/catalog') }}">Каталог</a>
                           <a href="{{ url('/sales') }}">Скидки</a>
                             <a href="{{ url('/information') }}">Полезная информация</a>
                           <a href="{{ url('/about-us') }}">О нас</a>
                        </div>
                        <div class="login-links">
                        @if (Route::has('login'))
                    
                    @auth
                        <a href="{{ url('/home') }}">Домой</a>
                    @else
                        <a href="{{ route('login') }}">Вход</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Регистрация</a>
                        @endif
                    @endauth
            
                 @endif    
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </header>
        <section id="all-products">
    <div class="content">
    <h2>Скидки</h2>
    <h3>Ноутбуки и ультрабуки</h3>
        <div class="products-main-block">
            <a href="/" class="product-item">
                <img src="{{asset('img/irbis-nout.jpg')}}" alt="">
                <p class="product-name">12.5" Нетбук Irbis NB125 синий</p>
                <p class="price">13 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/hp-nout.jpg')}}" alt="">
                <p class="product-name">15.6" Ноутбук HP 15-rb007ur черный</p>
                <p class="price">14 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/ir-wh.jpg')}}" alt="">
                <p class="product-name">14" Нетбук Irbis NB140 белый</p>
                <p class="price">15 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/asus-wh.jpg')}}" alt="">
                <p class="product-name">15.6" Ноутбук Asus VivoBook D540MA-GQ250 черный</p>
                <p class="price">17 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/acer-nout.jpg')}}" alt="">
                <p class="product-name">15.6" Ноутбук Acer Extensa EX215-21-43EZ черный</p>
                <p class="price">20 599 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/acer-green.jpg')}}" alt="">
                <p class="product-name">15.6" Ноутбук Acer Aspire 3 A315-22G-95AQ черный</p>
                <p class="price">29 499 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/lenovo-ultra.jpg')}}" alt="">
                <p class="product-name">14" Ультрабук Lenovo Ideapad S340-14IWL серый</p>
                <p class="price">29 799 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/hp-whh.jpg')}}" alt="">
                <p class="product-name">15.6" Ноутбук HP 15s-eq0008ur белый</p>
                <p class="price">29 899 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/dell-fiol.jpg')}}" alt="">
                <p class="product-name">13.3" Ультрабук Dell Inspiron 5390-8257 фиолетовый</p>
                <p class="price">50 899 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/vivo-book.jpg')}}" alt="">
                <p class="product-name">14" Ноутбук ASUS VivoBook X412UA-EB637T серебристый</p>
                <p class="price">51 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/tuf-nout.jpg')}}" alt="">
                <p class="product-name">15.6" Ноутбук ASUS TUF Gaming FX505DY-BQ103T черный</p>
                <p class="price">61 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/hp-game.jpg')}}" alt="">
                <p class="product-name">15.6" Ноутбук HP Pavilion Gaming 15-ec0035ur черный</p>
                <p class="price">63 999 ₽</p>
            </a>
        </div>
        <h3>Компьютеры</h3>
        <div class="products-main-block">
            <a href="/" class="product-item">
                <img src="{{asset('img/system-block-dexp.jpg')}}" alt="">
                <p class="product-name">ПК DEXP Mars E232</p>
                <p class="price">36 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/pc-acer-predator.jpg')}}" alt="">
                <p class="product-name">ПК Acer Predator PO3-600 DG.E14ER.002</p>
                <p class="price">122 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/pc-lenovo.jpg')}}" alt="">
                <p class="product-name">ПК Lenovo Legion T730-28ICO [90JF00EMRS]</p>
                <p class="price">158 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/pc-msi.jpg')}}" alt="">
                <p class="product-name">ПК MSI Infinite A 9SD-892RU [9S6-B91561-892]</p>
                <p class="price">165 999 ₽</p>
            </a>
        </div>
        <h3>Видеокарты</h3>
        <div class="products-main-block">
            <a href="/" class="product-item">
                <img src="{{asset('img/msi-vid.jpg')}}" alt="">
                <p class="product-name">Видеокарта MSI AMD Radeon RX 580 ARMOR OC [RX 580 ARMOR 8G OC]</p>
                <p class="price">14 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/vid-red.jpg')}}" alt="">
                <p class="product-name">Видеокарта KFA2 GeForce GTX 1660 SUPER Prodigy</p>
                <p class="price">19 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/vid-white.jpg')}}" alt="">
                <p class="product-name">Видеокарта KFA2 GeForce GTX 1660 EX WHITE [60SRH7DS04EK]</p>
                <p class="price">19 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/vid-three-white.jpg')}}" alt="">
                <p class="product-name">Видеокарта GIGABYTE GeForce RTX 2070 Super GAMING OC WHITE [N207SGAMINGOC WHITE-8GD]</p>
                <p class="price">50 999 ₽</p>
            </a>
        </div>
        <h3>ОЗУ</h3>
        <div class="products-main-block">
            <a href="/" class="product-item">
                <img src="{{asset('img/ozu-black.jpg')}}" alt="">
                <p class="product-name">Оперативная память Goodram IRDM X [IR-X2666D464L16S/4G] 4 ГБ</p>
                <p class="price">1 950 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/ozu-force.jpg')}}" alt="">
                <p class="product-name">Оперативная память Team Group T-Force Delta RGB [TF3D44G2666HC15B01] 4 ГБ</p>
                <p class="price">3 199 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/ozu-viper.jpg')}}" alt="">
                <p class="product-name">Оперативная память Patriot Viper Elite [PVE48G240C6GY] 8 ГБ</p>
                <p class="price">3 399 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/ozu-xpg.jpg')}}" alt="">
                <p class="product-name">Оперативная память A-Data XPG Gammix D10 [AX4U300038G16A-SB10] 8 ГБ</p>
                <p class="price">3 599 ₽</p>
            </a>
        </div>
        <h3>ПЗУ (SSD)</h3>
        <div class="products-main-block">
            <a href="/" class="product-item">
                <img src="{{asset('img/ssd-panter.jpg')}}" alt="">
                <p class="product-name">128 ГБ SSD-накопитель Apacer AS350 PANTHER [95.DB260.P100C]</p>
                <p class="price">2 499 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/ssd-gamer.jpg')}}" alt="">
                <p class="product-name">960 ГБ SSD-накопитель KFA2 Gamer L</p>
                <p class="price">11 399 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/ssd-cat.jpg')}}" alt="">
                <p class="product-name">480 ГБ SSD-накопитель Apacer AS340 PANTHER [AP480GAS340G-1]</p>
                <p class="price">5 450 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/ssd-nitro.jpg')}}" alt="">
                <p class="product-name">960 ГБ SSD-накопитель Seagate Nytro 1551 [XA960ME10063]</p>
                <p class="price">21 599 ₽</p>
            </a>
           
        </div>
        <h3>Мониторы</h3>
        <div class="products-main-block">
            <a href="/" class="product-item">
                <img src="{{asset('img/disp-dell.jpg')}}" alt="">
                <p class="product-name">19.5" Монитор Dell E2020H [2020-0674]</p>
                <p class="price">5 399 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/disp-philips.jpg')}}" alt="">
                <p class="product-name">21.5" Монитор Philips 223V7QDSB/00(01)</p>
                <p class="price">9 099 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/display-asus.jpg')}}" alt="">
                <p class="product-name">21.5" Монитор Asus VP228QG [90LM01K0-B06170]</p>
                <p class="price">11 899 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/disp-asus-mon.jpg')}}" alt="">
                <p class="product-name">37.5" Монитор ASUS MX38VC [90LM03B0-B01170]</p>
                <p class="price">119 999 ₽</p>
            </a>
        </div>
        <h3>Моноблоки</h3>
        <div class="products-main-block">
            <a href="/" class="product-item">
                <img src="{{asset('img/mono-lenovo.jpg')}}" alt="">
                <p class="product-name">21.5" Моноблок Lenovo IdeaCentre A340-22AST [F0EQ0067RK]</p>
                <p class="price">26 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/mono-dell.jpg')}}" alt="">
                <p class="product-name">21.5" Моноблок Dell Optiplex 5270 [5270-2172]</p>
                <p class="price">66 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/mono-lenovo-rgb.jpg')}}" alt="">
                <p class="product-name">23.8" Моноблок Lenovo V530-24ICB [10UX006PRU]</p>
                <p class="price">85 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/mono-elite.jpg')}}" alt="">
                <p class="product-name">34" Моноблок HP ProOne 1000 G2 [4PD92EA]</p>
                <p class="price">177 999 ₽</p>
            </a>
        </div>
       
        <h3>Материнские платы</h3>
        <div class="products-main-block">
            <a href="/" class="product-item">
                <img src="{{asset('img/mother-standart.jpg')}}" alt="">
                <p class="product-name">Материнская плата Asrock 760GM-HDV</p>
                <p class="price">3 299 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/mother-esonic.jpg')}}" alt="">
                <p class="product-name">Материнская плата Esonic G31CEL2</p>
                <p class="price">3 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/mother-gig.jpg')}}" alt="">
                <p class="product-name">Материнская плата GIGABYTE B450M GAMING</p>
                <p class="price">7 799 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/mother-msi.jpg')}}" alt="">
                <p class="product-name">Материнская плата MSI Z390 MPG GAMING EDGE AC</p>
                <p class="price">17 799 ₽</p>
            </a>
        </div>
        <h3>Процессоры</h3>
        <div class="products-main-block">
            <a href="/" class="product-item">
                <img src="{{asset('img/proc-amd.jpg')}}" alt="">
                <p class="product-name">Процессор AMD Athlon X4 840 OEM</p>
                <p class="price">1 999 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/proc-ryzen.jpg')}}" alt="">
                <p class="product-name">Процессор AMD Ryzen 7 2700 OEM</p>
                <p class="price">13 499 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/proc-intel.jpg')}}" alt="">
                <p class="product-name">Процессор Intel Core i5-9400 OEM</p>
                <p class="price">17 599 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/proc-xeon.jpg')}}" alt="">
                <p class="product-name">Процессор Intel Xeon E5-2650 v4 OEM</p>
                <p class="price">103 499 ₽</p>
            </a>
        </div>
        <h3>Системы охлаждения</h3>
        <div class="products-main-block">
            <a href="/" class="product-item">
                <img src="{{asset('img/cul-dep.jpg')}}" alt="">
                <p class="product-name">Кулер для процессора DEEPCOOL Beta 10 [DP-ACAL-B10]</p>
                <p class="price">570 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/water-system.jpg')}}" alt="">
                <p class="product-name">Система охлаждения ID-Cooling AURAFLOW X 240</p>
                <p class="price">5 299 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/water-red.jpg')}}" alt="">
                <p class="product-name">Система охлаждения Gamer Storm CAPTAIN 120EX</p>
                <p class="price">5 399 ₽</p>
            </a>
            <a href="/" class="product-item">
                <img src="{{asset('img/cul-rgb.png')}}" alt="">
                <p class="product-name">Вентилятор Aerocool Frost 12 PWM RGB [ACF3-FS11117.11]</p>
                <p class="price">650 ₽</p>
            </a>
        </div>
    </div>
</section>

<footer>
    <div class="content">
        <div class="footer-content">
            <div class="footer-column">
                <h4>Навигация</h4>
                <ul>
                    <li>
                        <a href="">Каталог</a>
                    </li>
                    <li>
                        <a href="">Скидки</a>
                    </li>
                    <li>
                        <a href="">Полезная информация</a>
                    </li>
                    <li>
                        <a href="">О нас</a>
                    </li>
                </ul>
            </div>
            <div class="footer-column">
                <h4>Авторизация</h4>
                
                <ul>
                @if (Route::has('login'))
                    
                    @auth
                    <li>
                        <a href="{{ url('/home') }}">Домой</a>
                        </li>
                    @else
                    <li>
                        <a href="{{ route('login') }}">Вход</a>
                        </li>

                        @if (Route::has('register'))
                        <li>
                            <a href="{{ route('register') }}">Регистрация</a>
                            </li>
                        @endif
                    @endauth
            
                 @endif    
                </ul>
            </div>
            <div class="footer-column column-razr">
               <div class="razr">
                   <p>Разработка сайта</p>
                   <p>Кузнецов Степан 2020</p>
               </div>
            </div>
        </div>
    </div>
</footer>




       
        
    </body>
</html>
