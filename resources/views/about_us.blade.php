
<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

       <title>МЕГАКОМП</title><link rel="shortcut icon" type="image/png" href="{{asset('img/fav.png')}}"/>
        <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('js/loader.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/mainpage.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/accordeon.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/product.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/minheight.js')}}"></script>

        <!-- Fonts -->
        <link rel="stylesheet" href="{{asset('styles/style.css')}}">
        <link href="{{asset('Montserrat/stylesheet-montserrat.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('styles/loader.css')}}">
        <link rel="stylesheet" href="{{asset('styles/product.css')}}">
    </head>
    
    <body>
        

        <header>
        <div class="loaderArea">
        <div class="sk-circle" id="preloader">
            <div class="sk-circle1 sk-child"></div>
            <div class="sk-circle2 sk-child"></div>
            <div class="sk-circle3 sk-child"></div>
            <div class="sk-circle4 sk-child"></div>
            <div class="sk-circle5 sk-child"></div>
            <div class="sk-circle6 sk-child"></div>
            <div class="sk-circle7 sk-child"></div>
            <div class="sk-circle8 sk-child"></div>
            <div class="sk-circle9 sk-child"></div>
            <div class="sk-circle10 sk-child"></div>
            <div class="sk-circle11 sk-child"></div>
            <div class="sk-circle12 sk-child"></div>
        </div>
    </div>
            <a href="{{ url('/') }}" class="top-title-header">
                <div class="content">
                    <div class="title-header">
                <h1>МЕГАКОМП</h1>
                <p class="sub-title-header">
                    Интернет-магазин компьютерной техники
                </p>
                </div>
                </div>
            </a>
            <div class="full-width-header">
            <div class="content">
                <div class="header-content">
                        <div class="links">
                            <a href="{{ url('/catalog') }}">Каталог</a>
                           <a href="{{ url('/sales') }}">Скидки</a>
                             <a href="{{ url('/information') }}">Полезная информация</a>
                           <a href="{{ url('/about-us') }}">О нас</a>
                        </div>
                        <div class="login-links">
                        @if (Route::has('login'))
                    
                    @auth
                        <a href="{{ url('/home') }}">Домой</a>
                    @else
                        <a href="{{ route('login') }}">Вход</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Регистрация</a>
                        @endif
                    @endauth
            
                 @endif    
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </header>
<section id="product-all-info">

    <div class="content about-us-content">
       <p class="about-us-item">
       Мы рады приветствовать Вас в нашем магазине! Если Вы попали на нашу страничку, значит скорее всего вы искали "купить комплектующие для компьютера" и Вы попали по нужному адресу!</p>
       <p class="about-us-item">
Компания «Мега Комп» на Российском рынке по продаже компьютерной техники начала не так давно, в 2018 году. Но уже успела зарекомендовать себя среди тысяч покупателей.</p>
<p class="about-us-item">
Мы предлагаем своим покупателям приобрести комплектующие для компьютеров, ноутбуки, готовые сборки пк по одной из самых выгодных цен на рынке. Покупая у нас Вы гарантированно получаете качество всего перечня товара представленного в нашем интерне-магазине!</p>
<p class="about-us-item">
Весь представленный ассортимент товаров в нашем магазине поступает к нам исключительно от производителя и хранится на нашем складе, что дает вам возможность купить комплектующее без длительных ожиданий доставки. Товар будет доставлен вам в течении 1-2 дней, при условии что он имеется в наличии. Если товара в наличии нет, сроки доставки увеличиваются от 3 до 5 рабочих дней.</p>
<p class="about-us-item">
Так же мы постоянно расширяем каталог наших товаров, следим за выходом новинок в мире компьютерной индустрии, что бы Вы смогли купить самое мощное железо для своего компьютера.</p>
<p class="about-us-item">
Для постоянных клиентов у нас предусмотрена бонусная программа, при первом заказе в нашем магазине вы получите нашу карту, в дальнейшем при заказе товара вам будет начисляться 10% бонусов от товара.</p>
<p class="about-us-item">
1 бонус = 1 рублю
Тем самым вы сможете оплатить всю стоимость товара бонусами.</p>
<p class="about-us-item">
У нас индивидуальный подход к каждому покупателю в отдельности.
Мы ценим Вас и Ваше время, с уважением, МЕГАКОМП!</p>
    </div>
</section>

<footer>
    <div class="content">
        <div class="footer-content">
            <div class="footer-column">
                <h4>Навигация</h4>
                <ul>
                    <li>
                        <a href="">Каталог</a>
                    </li>
                    <li>
                        <a href="">Скидки</a>
                    </li>
                    <li>
                        <a href="">Полезная информация</a>
                    </li>
                    <li>
                        <a href="">О нас</a>
                    </li>
                </ul>
            </div>
            <div class="footer-column">
                <h4>Авторизация</h4>
                
                <ul>
                @if (Route::has('login'))
                    
                    @auth
                    <li>
                        <a href="{{ url('/home') }}">Домой</a>
                        </li>
                    @else
                    <li>
                        <a href="{{ route('login') }}">Вход</a>
                        </li>

                        @if (Route::has('register'))
                        <li>
                            <a href="{{ route('register') }}">Регистрация</a>
                            </li>
                        @endif
                    @endauth
            
                 @endif    
                </ul>
            </div>
            <div class="footer-column column-razr">
               <div class="razr">
                   <p>Разработка сайта</p>
                   <p>Кузнецов Степан 2020</p>
               </div>
            </div>
        </div>
    </div>
</footer>




       
        
    </body>
</html>
