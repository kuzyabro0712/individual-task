document.addEventListener("DOMContentLoaded", function (event) {
/*Получаем id страницы через обрезку строки*/
let pageId = window.location.pathname.substr(6)

/*Создаем элементы*/    
let all_block_info = document.createElement('div');
let product_block = document.createElement('div');
let info_image_block = document.createElement('div');
let img_image_item = document.createElement('img');
let buy_and_price_block = document.createElement('div');
let info_product_name = document.createElement('h2');
let info_product_code = document.createElement('p');
let info_type = document.createElement('p');
let info_price = document.createElement('p');
let in_stock = document.createElement('p');
let buy_button = document.createElement('div');
let buy_p = document.createElement('p');
let buy_img = document.createElement('img');
let characters_info = document.createElement('div');
let characters_info_image = document.createElement('img');
let main__content_item = document.createElement('div');
let main__content_title = document.createElement('p');
let subcontent = document.createElement('div');
let addBlock = document.querySelector('.add-content')

/*Присваиваем им классы*/ 
all_block_info.className = 'all-block-info';
product_block.className ='product-block';
info_image_block.className ='info-image-block';
buy_and_price_block.className ='buy-and-price-block';
info_product_name.className ='info-product-name';
info_product_code.className ='info-product-code';
info_type.className ='info-type';
info_price.className ='info-price';
in_stock.className ='in-stock';
buy_button.className ='buy-button';
characters_info.className ='characters-info';
characters_info_image.className = 'arrow';
main__content_item.className = 'main__content-item';
main__content_title.className = 'main__content-title';
subcontent.className = 'subcontent';

/*Заполняем элементы*/ 
img_image_item.setAttribute('src', '../../'+getArrApi(pageId, ['img'])+'');
info_product_name.innerText = getArrApi(pageId, ['Название']);
info_product_code.innerText = getArrApi(pageId, ['Код']);
info_type.innerText = getArrApi(pageId, ['Тип']);
info_price.innerText = getArrApi(pageId, ['Цена']);
in_stock.innerText ='В наличии: '+getArrApi(pageId, ['В наличии']);
buy_p.innerText='Купить';
buy_img.setAttribute('src', '../../img/korzina.svg');
characters_info_image.setAttribute('src', '../../img/line5.svg');
main__content_title.innerText = 'Характеристики';

/*Добавляем заполненные элементы в DOM*/ 
let contentBlock = document.querySelector('.add-content');
contentBlock.appendChild(all_block_info);
all_block_info.appendChild(product_block);
product_block.appendChild(info_image_block);
product_block.appendChild(buy_and_price_block);
info_image_block.appendChild(img_image_item);
buy_and_price_block.appendChild(info_product_name);
buy_and_price_block.appendChild(info_product_code);
buy_and_price_block.appendChild(info_type);
buy_and_price_block.appendChild(info_price);
buy_and_price_block.appendChild(in_stock);
buy_and_price_block.appendChild(buy_button);
buy_button.appendChild(buy_p);
buy_button.appendChild(buy_img);
characters_info.appendChild(main__content_item);
main__content_item.appendChild(main__content_title);
main__content_item.appendChild(characters_info_image);
for (let key in getFakeArrApi()['product'+pageId]){
    
    
   
    if(key!='img'){
        let parametr = document.createElement('p'); 
       
        parametr.className = 'parametr';
       
        parametr.innerText = key;
        subcontent.appendChild(parametr);
       }
       else{
    
       
       }
    

   
   if(getFakeArrApi()['product'+pageId][key]!=getFakeArrApi()['product'+pageId]['img']){
    let paravetrValue =  document.createElement('p');
    paravetrValue.className = 'parametr-value';
    paravetrValue.innerText = getFakeArrApi()['product'+pageId][key]
    subcontent.appendChild(paravetrValue);
 
   }
   else{

   
   }
       
    
       
    
  

  
}
main__content_item.appendChild(subcontent);



addBlock.appendChild(characters_info);












/*Выпадающий алерт */

let hdr = document.querySelector('header');
hdr.style.transition = '0.5s'
let ftr = document.querySelector('footer');
ftr.style.transition = '0.5s'
let prodSection = document.querySelector('section');
prodSection.style.transition = '0.5s'

let alertBlock = document.querySelector('.alert-buy-block')
let closeBtn = document.querySelector('.close-button')
let blackBackground = document.querySelector('.full-width-block')
buy_button.onclick = function(event){
    alertBlock.style.top = '0';
    blackBackground.style.height = '100vh';
}
closeBtn.onclick = function(event){
    alertBlock.style.top = '-100%';
  
    blackBackground.style.height = '0';
  
}

checkedQuiz();

    // console.log(getArrApi(43, 'code')) 
 },false);
 
 
 function getArrApi(id, field){
     let request = new XMLHttpRequest();
     request.open('GET', '../mainapi.php?test=test', false);
     request.send()
 
     if (request.status == 200) {
     
         let send__parse = JSON.parse(request.response)
      
      
         return(send__parse['product'+id][field])
     } else {
         console.log('Error')
     }
 
 
 }
 function getFakeArrApi(id){
    let request = new XMLHttpRequest();
    request.open('GET', '../mainapi.php?test=test', false);
    request.send()
    let computer = {
        name: 'Компьютер',
        size: 50,
        notebook: false,
        GPU: [' NVIDIA GeForce RTX 2080 Ti', ' NVIDIA GeForce RTX 2080 Ti', 'NVIDIA Quadro RTX 6000'],
        items: [{itemName:'Процессор', frequency: 2.77, numberofcores:'4', numberofthreads:'8',designcapacity:'65w', cachememory: '8 MB Intel® Smart Cache'}]
        };
        
        let json = JSON.stringify(computer );
        console.log(typeof json);
        console.log(computer );
        

    if (request.status == 200) {
    
        let send__parse = JSON.parse(request.response)
        
     

        return(send__parse)
    } else {
        console.log('Error')
    }


}


 function checkedQuiz(){
   


    let submitBtnsToCookie = document.querySelectorAll('.buy-button');
    
    for (let i=0; i<submitBtnsToCookie.length; i++){
        submitBtnsToCookie[i].onmousedown = function(event){
            
            if (localStorage.getItem('foo')==null||localStorage.getItem('myMap')==null){
                let map = new Map([[getArrApi(window.location.pathname.substr(6), ['Название']), getArrApi(window.location.pathname.substr(6), ['Цена'])]]);
                localStorage.myMap = JSON.stringify(Array.from(map.entries())); 
                foo = [getArrApi(window.location.pathname.substr(6), ['Название']), getArrApi(window.location.pathname.substr(6), ['Цена'])];
                localStorage.foo = JSON.stringify(foo);
                if(JSON.parse(localStorage.foo)||JSON.parse(localStorage.myMap)){
                    let map = localStorage.myMap;
                    foo = localStorage.foo;
                }
                else{
                    [];
                }
            }
            else{
                map = JSON.parse(localStorage.myMap);
                map.push([getArrApi(window.location.pathname.substr(6), ['Название']), getArrApi(window.location.pathname.substr(6), ['Цена'])]);
                localStorage.myMap = JSON.stringify(map);
                foo = JSON.parse(localStorage.foo);
                foo.push(location.href);
                localStorage.foo = JSON.stringify(foo);
                if(JSON.parse(localStorage.foo)||JSON.parse(localStorage.myMap)){
                    foo = localStorage.foo;
                    map = localStorage.myMap;
                }
                else{
                    [];
                }
            }
        }
    }
}



